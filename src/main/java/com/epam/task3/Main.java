package com.epam.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("");
        Main main = new Main();
        List <Integer> numbers = main.generateList(10);
        double average = numbers.stream().mapToDouble(Integer::intValue).average().getAsDouble();
        IntSummaryStatistics statistics = numbers.stream().mapToInt(Integer::intValue).summaryStatistics();

        logger.info("List: " + numbers);
        logger.info("Average value: " + average);
        logger.info("Min value: " + statistics.getMin());
        logger.info("Max value: " + statistics.getMax());
        logger.info("Sum by statistic: " + statistics.getSum());
        logger.info("Sum using reduce and sum: " + numbers.stream()
                .mapToInt(Integer::intValue)
                .reduce((a,b)->a+b)
                .getAsInt());
        logger.info("Count number of values that are bigger than average: " + numbers.stream()
                .filter((a) -> a > average)
                .count());
    }

    private List<Integer> generateList(int n) {
        Random random = new Random();
        return Stream.generate(() -> random.nextInt(20))
                                            .limit(n)
                                            .collect(Collectors.toList());
    }
}
