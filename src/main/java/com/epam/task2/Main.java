package com.epam.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static Logger logger = LogManager.getLogger(com.epam.task1.Main.class);

    public static void main(String[] args) {
        Command command;
        Scanner scanner = new Scanner(System.in);
        logger.info("Menu:\n" +
                "1 - Command as lambda function;\n" +
                "2 - Command as method reference;\n" +
                "3 - Command as anonymous class;\n" +
                "4 - Command as object of command class.\n" +
                "Choose menu item:");
        int input = scanner.nextInt();
        switch (input) {
            case 1:
                command = s -> logger.info(s);
                command.execute("Command as lambda function");
                break;
            case 2:
                command = new CommandImpl()::execute;
                command.execute("Command as method reference");
                break;
            case 3:
                command = new Command() {
                    @Override
                    public void execute(String s) {
                        logger.info(s);
                    }
                };
                command.execute("Command as anonymous class");
                break;
            case 4:
                command = new CommandImpl();
                command.execute("Command as object of command class");
                break;
            default:
                logger.info("Wrong input!");
        }
    }
}
