package com.epam.task2;

@FunctionalInterface
public interface Command {
    void execute(String s);
}
