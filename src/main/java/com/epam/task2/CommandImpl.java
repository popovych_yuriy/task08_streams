package com.epam.task2;

public class CommandImpl implements Command {
    @Override
    public void execute(String s) {
        System.out.println(s);
    }
}
