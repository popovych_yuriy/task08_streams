package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Main main = new Main();
        String line;
        List<String> lines = new ArrayList<>();

        try (Scanner scanner = new Scanner(System.in)) {
            logger.info("Input lines (empty line to exit):");
            while (!(line = scanner.nextLine()).isEmpty()) {
                lines.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("All lines: " + lines);
        logger.info("Number of unique words: " + main.countUnique(lines));
        logger.info("Sorted list of all unique words: " + main.sortUnique(lines));
        logger.info("Word count: "+main.countWords(lines));
        logger.info("Occurrence number of each symbol except upper case characters: "+main.countSymbols(lines));
    }

    private long countUnique(List<String> lines) {
        return lines.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .distinct()
                .count();
    }

    private List<String> sortUnique(List<String> lines) {
        return lines.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    private Map<String, Long> countWords(List<String> lines) {
        return lines.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .collect(Collectors.groupingBy(String::valueOf, Collectors.counting()));
    }

    private Map<String, Long> countSymbols(List<String> lines) {
        return lines.stream()
                .flatMap(line -> Stream.of(line.split(" ")))
                .flatMap(line -> Stream.of(line.split("")))
                .filter(ch -> !Character.isUpperCase(ch.charAt(0)))
                .collect(Collectors.groupingBy(String::valueOf, Collectors.counting()));

    }
}
