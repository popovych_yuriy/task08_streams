package com.epam.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Input values: 4, 6, 3");

        Functional maxValue = (a, b, c) -> (a > b) && (a > c) ? a : (b > c) ? b : c;
        logger.info("Max value: " + maxValue.execute(4, 6, 3));

        Functional averageValue = (a, b, c) -> (a + b + c) / 3;
        logger.info("Average value: " + averageValue.execute(4, 6, 3));

    }
}
