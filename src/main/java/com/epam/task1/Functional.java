package com.epam.task1;

@FunctionalInterface
public interface Functional {
    int execute(int a, int b, int c);
}
